﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Metier;

namespace Synapse.Metier
{
    [Serializable()]
    public class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMO;
        private List<Mission> _missions;
        #region CONSTRUCTEUR
        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }
        public DateTime Debut
        {
            get { return _debut; }
            set { _debut = value; }
        }
        public DateTime Fin
        {
            get { return _fin; }
            set { _fin = value; }
        }
        public decimal PrixFactureMO
        {
            get { return _prixFactureMO; }
            set { _prixFactureMO = value; }
        }
        #endregion

        
       

        private decimal CumulCoutMO()
        {
            decimal cumul = 0;
            foreach (Mission m in _missions)
            {
                cumul += m.NbHeuresEffectues() * m.GetExecutant().GetTauxHoraire();
            }
            return cumul;
        }

        public decimal MargeBruteCourant()
        {
            decimal margeBrute = 0;
            margeBrute = CumulCoutMO() - PrixFactureMO;
            return margeBrute;
        }
    }
}
