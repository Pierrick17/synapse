﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Metier;

namespace Synapse.Metier
{
    [Serializable()]
    public class Intervenant
    {
        private string _nom;
        private decimal _tauxHoraire;

        #region CONSTRUCTEUR
        
       

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }
        public decimal TauxHoraire
        {
            get { return _tauxHoraire; }
            set { _tauxHoraire = value; }
        }
        #endregion
        
        public decimal GetTauxHoraire()
        {
            return _tauxHoraire;
        }
    }
}
