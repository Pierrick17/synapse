﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Synapse.Utilitaire
{
    public static class Persistance
    {
        public static string repertoireApplication = Environment.CurrentDirectory + @"\";

        public static List<Projet> ChargeProjet()
        {
            FileStream fs = null;
            List<Projet> projet = new List<Projet>();

            try
            {

                fs = new FileStream(repertoireApplication + Projet, FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    projet = (IList)formatter.Deserialize(fs);
                }
                catch(SerializationException err)
                {
                    Persistance.FichierLog(err);
                }
                finally
                {
                    fs.Close;
                }
            }
            catch(Exception erreur)
            {

            }
            return projet;
        }

        public static void SauvegarderProjet(List<Projet> projet)
        {
            FileStream file = null;

            try
            {
                file = File.Open(repertoireApplication + projet, FileMode.OpenOrCreate);
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, projet);
            }
            catch(Exception err)
            {
                Persistance.FichierLog(err);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }

        public static List<LesIntervenants> ChargeIntervenant()
        {
            FileStream fs = null;
            List<LesIntervenants> intervenants = new List<LesIntervenants>();

            try
            {

                fs = new FileStream(repertoireApplication + Intervenants, FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    projet = (IList)formatter.Deserialize(fs);
                }
                catch (SerializationException err)
                {
                    Persistance.FichierLog(err);
                }
                finally
                {
                    fs.Close;
                }
            }
            catch (Exception erreur)
            {

            }
            return intervenants;
        }

        public static void SauvegarderIntervenant(List<LesIntervenants> intervenants)
        {
            FileStream file = null;

            try
            {
                file = File.Open(repertoireApplication + Intervenants, FileMode.OpenOrCreate);
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, projet);
            }
            catch (Exception err)
            {
                Persistance.FichierLog(err);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
    }
}
